import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { RouteReuseStrategy } from '@angular/router';
import { RouteReuseService } from './shared/services/route-reuse/route-reuse.service';

@NgModule({
  declarations: [AppComponent, NotFoundComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: RouteReuseService,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

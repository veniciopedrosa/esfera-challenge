import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../shared/services/user/user.service';

export interface UserInterface {
  id: number;
  name: string;
  username: string;
  email: string;
  address: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: {
      lat: string;
      lng: string;
    };
  };
  phone: string;
  website: string;
  company: {
    name: string;
    catchPhrase: string;
    bs: string;
  };
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  public users: UserInterface[];

  constructor(private router: Router, private service: UserService) {}

  ngOnInit(): void {
    this.service.gerUsers().subscribe((data) => {
      this.users = data;
    });
  }

  public open(id: string) {
    this.router.navigate(['/', id], { skipLocationChange: true });
  }
}

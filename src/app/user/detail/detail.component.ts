import { Component, OnInit, HostListener } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { UserService } from '../../shared/services/user/user.service';
import { UserInterface } from '../list/list.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  public user: UserInterface;
  public lat: number;
  public lng: number;
  public zoom = 2;

  @HostListener('window:popstate', ['$event'])
  onBrowserBackBtnClose(event: Event) {
    console.log('back button pressed');
    event.preventDefault();
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: UserService
  ) {}

  ngOnInit(): void {
    this.route.params
      .pipe(
        switchMap((p: Params) => {
          return this.service.getUser(p.id);
        })
      )
      .subscribe((data) => {
        if (data) {
          this.user = data;
          this.lat = +data.address.geo.lat;
          this.lng = +data.address.geo.lng;
        } else {
        }
      });
  }
}

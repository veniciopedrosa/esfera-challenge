import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { UserInterface } from 'src/app/user/list/list.component';
import { map, switchMap, find } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private url = 'https://jsonplaceholder.typicode.com/users';

  constructor(private http: HttpClient) {}

  public gerUsers(): Observable<UserInterface[]> {
    return this.http.get(this.url).pipe(map((res: UserInterface[]) => res));
  }

  public getUser(id: number): Observable<UserInterface> {
    return this.gerUsers().pipe(
      map((val) => {
        return val.find((u) => u.id === +id);
      })
    );
  }
}
